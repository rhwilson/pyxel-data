{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{admonition} Live Notebook\n",
    "You can run this notebook in a \n",
    "[live session ![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?labpath=examples%2Fmodels%2Finter_pixel_capacitance%2Fipc.ipynb)\n",
    "or view it on [Gitlab](https://gitlab.com/esa/pyxel-data/-/blob/master/examples/models/inter_pixel_capacitance/ipc.ipynb).\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Inter-pixel capacitance model\n",
    "\n",
    "This notebook is an example of using the simple inter-pixel capacitance (IPC) model for simulation of CMOS detectors. For installation instructions and basic use of `Pyxel` refer to the basic example."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How it works\n",
    "\n",
    "When there is IPC, the signal read out on any pixel is affected by the signal in neighboring pixels. The IPC affects the point spread function (PSF) of the optical system, modiying the shape of the objects. More about the IPC and the math describing it can be found in the following article: https://iopscience.iop.org/article/10.1088/1538-3873/128/967/095001/pdf. The amount of coupling between the pixels is described in the article by a $3\\times3$ matrix $K_{\\alpha, \\alpha_+, \\alpha'}$:\n",
    "\n",
    "$K_{\\alpha, \\alpha_+, \\alpha'} = \\begin{bmatrix}\n",
    "\\alpha' & \\alpha-\\alpha_+ & \\alpha'\\\\\n",
    "\\alpha+\\alpha_+ & 1-4(\\alpha+\\alpha') & \\alpha+\\alpha_+\\\\\n",
    "\\alpha' & \\alpha-\\alpha_+ & \\alpha'\n",
    "\\end{bmatrix},$\n",
    "\n",
    "where $\\alpha$ is the coupling parameter for the neighbouring pixels, $\\alpha'$ the coupling parameter for the pixels located on the diagonals and $\\alpha_+$ parameter for introducing an anisotropical coupling. In the model, the last two are optional. The sum of the matrix elements is always $1$. The result image that is seen on the detector is a convolution of the image with the kernel matrix, which is done using astropy convolution tools.\n",
    "\n",
    "An example of YAML code for the model in the charge collection stage would look like this:\n",
    "```yaml\n",
    "- name: simple_ipc\n",
    "  func: pyxel.models.charge_collection.simple_ipc\n",
    "  enabled: true\n",
    "  arguments:\n",
    "      coupling: 0.1\n",
    "      diagonal_coupling: 0.05\n",
    "      anisotropic_coupling: 0.03\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import pyxel\n",
    "from pyxel.models.charge_collection.inter_pixel_capacitance import ipc_kernel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example of an IPC kernel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coupling = 0.1\n",
    "diagonal_coupling = 0.05\n",
    "anisotropic_coupling = 0.03"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kernel = ipc_kernel(coupling, diagonal_coupling, anisotropic_coupling)\n",
    "plt.imshow(kernel)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Usage with `Pyxel`\n",
    "\n",
    "Let's see with Pyxel how IPC changes shapes of some stars."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Open the example YAML file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"ipc.yaml\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating detector and detection pipeline objects\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "exposure = config.exposure\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Displaying a single model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(pipeline.charge_collection.simple_ipc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Processing the detector through the pipeline\n",
    "\n",
    "Now that the configuration is loaded, we have a detection pipeline object and a detector object. To start the exposure mode pipeline, we can run it by calling the function `exposure_mode`, passing the detector, pipeline and the instance of `Exposure` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.exposure_mode(exposure=exposure, detector=detector, pipeline=pipeline)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display detector\n",
    "\n",
    "The result of a convolution with the IPC kernel is seen as objects appear more blurry."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_detector(detector)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
