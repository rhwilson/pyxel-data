{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "53c09fec-c65c-487e-a9ec-7f5a36b95a09",
   "metadata": {},
   "source": [
    "# Dark current vs temperature for MCT detectors\n",
    "\n",
    "In this notebook we use Pyxel in observation mode and visualize the results of the dark current versus temperature for rule07.\n",
    "If one or more values for the fixed pattern noise factor are given it will include the Dark Signal Non Uniformity.\n",
    "\n",
    "Check also the [Documentation](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/charge_generation_models.html#dark-current)\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Tip:</b> Open the <i>observation_rule07.yaml</i> file in parallel for a better overview.</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a505cd0d-09ca-4b85-badc-a51702bfa357",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a4ef7c0-99ec-4732-8d99-ebfc5f4829a3",
   "metadata": {},
   "source": [
    "## Loading the configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7d19178-a16b-4af6-83d5-33178ad56cc3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a configuration class from the yaml file\n",
    "config = pyxel.load(\"observation_rule07.yaml\")\n",
    "\n",
    "observation = config.observation  # class Observation\n",
    "detector = config.detector  # class CMOS\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "957dc166-e3fc-4942-98d5-f299cee31538",
   "metadata": {},
   "source": [
    "## Running the pipelines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "478813e2-7a5e-4150-b959-70ccb0aadcc3",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.observation_mode(\n",
    "    observation=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "829f285d-01e4-4bab-b3c4-0fba67556212",
   "metadata": {},
   "outputs": [],
   "source": [
    "result.dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e143a5b-e0fb-4616-af16-5b8791fdee8c",
   "metadata": {},
   "source": [
    "## Plotting the outputs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4fef672-0d19-45a4-aec3-b9eddcb72819",
   "metadata": {},
   "source": [
    "The plot will show the dark current model rule 07 used in Pyxel against the temperature for different fixed pattern noise (FPN) values. The dark current figure of merit at 300 K is 0.01 nA/cm^2.\n",
    "Higher FPN values result in higher dark current values, while all calculations converge for temperatures higher than 180 K.\n",
    "\n",
    "You can set temporal noise to **true** in the yaml file, run everything again and see the difference in the plot, if temporal noise is included. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e02308d2-95d6-40ee-9c42-ab5a505b23ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a72824c-03e6-46a7-96d0-9eb9da7c703f",
   "metadata": {},
   "outputs": [],
   "source": [
    "dark_current = result.dataset.pixel.mean(dim=[\"x\", \"y\"]).to_numpy()\n",
    "temperature = result.dataset.temperature.to_numpy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "439dc9db-a0b0-4aef-97a9-aef0346385c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(1)\n",
    "for i in range(0, len(result.dataset.spatial_noise_factor)):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current[i],\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"FPN factor: {result.dataset.spatial_noise_factor.values[i]}\",\n",
    "    )\n",
    "plt.title(\"Dark current vs. temperature\")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.xlim([140, 200])\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ff26018-791b-4f2f-8de7-8ef58bfd62c4",
   "metadata": {},
   "source": [
    "## Plotting image as function of readout time, temperature and figure of merit\n",
    "\n",
    "Library `holoviews` can be used to quickly plot data stored inside `xarray` datasets.\n",
    "Here with the `bokeh` backend, `matplotlib` is also supported.\n",
    "\n",
    "You can use the toolbar to change temperature and figure of merit to see the influence on the detector.\n",
    "\n",
    "Try and change the readout time in the configuration file, reload the configuration and run the pipeline again. Then display the results and see the influence of the readout time.\n",
    "\n",
    "Longer readout times result in higher dark current and saturation is reached at lower temperatures already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9bf778cc-684c-420f-8df7-324068bf580e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts\n",
    "\n",
    "hv.extension(\"bokeh\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1b5097e-fc21-4f35-92ec-edf7b310feae",
   "metadata": {},
   "outputs": [],
   "source": [
    "out_1a = hv.Dataset(result.dataset[\"image\"])\n",
    "plot_1a = out_1a.to(hv.Image, [\"x\", \"y\"], dynamic=True)\n",
    "plot_1a.opts(opts.Image(aspect=1, cmap=\"gray\", tools=[\"hover\"]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a22ef7ea-ed12-446f-a2b2-d97e7ded039f",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9bb1e85-28b9-4f25-a70b-098768d47164",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca275678-07f0-4c02-a290-321af27afcb6",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "toc-autonumbering": true,
  "toc-showcode": true,
  "toc-showmarkdowntxt": true,
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {
     "0a735b122b384c13878c173b641a991c": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "ProgressStyleModel",
      "state": {
       "description_width": ""
      }
     },
     "73bcec173adb410ca33a573c56a8f39d": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_e35b6ef1a90a43979b6d00667f83fb98",
       "style": "IPY_MODEL_8883590b16fe4fd0baa3805b21bb5697",
       "value": "100%"
      }
     },
     "8883590b16fe4fd0baa3805b21bb5697": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "96c61adaf7f9485fb9b9a57d5339aeda": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_b987ddc3df2e430f9d72a6d95f1cf3c1",
       "style": "IPY_MODEL_98fef3ff580e41abb199ad3651b982e8",
       "value": " 90/90 [00:00&lt;00:00, 132.49it/s]"
      }
     },
     "98fef3ff580e41abb199ad3651b982e8": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "b18b3406ff53414d9fab28f43e532748": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "FloatProgressModel",
      "state": {
       "bar_style": "success",
       "layout": "IPY_MODEL_fab02ff20a7649ffbc97bc26db09337e",
       "max": 90,
       "style": "IPY_MODEL_0a735b122b384c13878c173b641a991c",
       "value": 90
      }
     },
     "b987ddc3df2e430f9d72a6d95f1cf3c1": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "cecb8ff9a4f34c018c976e140f3b21e7": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HBoxModel",
      "state": {
       "children": [
        "IPY_MODEL_73bcec173adb410ca33a573c56a8f39d",
        "IPY_MODEL_b18b3406ff53414d9fab28f43e532748",
        "IPY_MODEL_96c61adaf7f9485fb9b9a57d5339aeda"
       ],
       "layout": "IPY_MODEL_f012ea37f55c4fb28482a03fce08a8cd"
      }
     },
     "e35b6ef1a90a43979b6d00667f83fb98": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "f012ea37f55c4fb28482a03fce08a8cd": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "fab02ff20a7649ffbc97bc26db09337e": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     }
    },
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
