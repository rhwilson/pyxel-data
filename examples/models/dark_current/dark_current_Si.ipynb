{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "53c09fec-c65c-487e-a9ec-7f5a36b95a09",
   "metadata": {},
   "source": [
    "# Dark current versus temperature for silicon detectors\n",
    "\n",
    "In this notebook we will see the dark current against temperature for Si detectors with the influence of two different **noise sources** (at different readout times):\n",
    "- Spatial noise: Fixed-pattern noise (FPN)\n",
    "- Temporal noise: Shot noise\n",
    "\n",
    "We use Pyxel in observation mode by changing the following parameters:\n",
    "1. Temperature from detector operating temperature up to room temperature in K\n",
    "2. Dark current at room temperature = dark current figure of merit at 300 K between 0.01 and 1000 nA/cm^2\n",
    "3. Dark current FPN factor is typically between 0.1 and 0.4, while with FPN=0 no spatial noise is included,\n",
    "\n",
    "and comparing the result in Pyxel with two approximations from datasheets.\n",
    "\n",
    "The dark current model used in Pyxel is adapted from this book:\n",
    "[\"Scientific Charge-Couple Devices.\" by Janesick, J. (2001)](https://spie.org/Publications/Book/374903?SSO=1).\n",
    "Typical values are taken from this paper:\n",
    "[\"High-level numerical simulations of noise in CCD and CMOS photosensors\" by Konnik, M. and Welsh, J. (2014)](https://arxiv.org/abs/1412.4031).\n",
    "Check the dark current model description in the [Pyxel Documentation](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/charge_generation_models.html#dark-current)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adb68152-ea10-4bd9-89fd-6a4d9064b4f3",
   "metadata": {},
   "source": [
    "# Observation mode\n",
    "\n",
    "<img style=\"float: center;\" src=\"images/observation.png\" width=\"300\">\n",
    "\n",
    "In observation mode, the pipeline is run multiple times with different parameters, specified by the user in the configuration file. We use the library `Xarray`  to save output data for each parameter in a dataset. User can change model parameters as well as detector parameters. Parameters can be specified in the configuration file or loaded from file. The library `matplotlib` is used for visualisation of the results.\n",
    "\n",
    "By the end of this notebook you will know how to:\n",
    "* Run observation mode\n",
    "* Do changes in the configuration file\n",
    "* Inspect and save the observation results\n",
    "\n",
    "\n",
    "**Read more** about the observation mode in the [Pyxel Documentation](https://esa.gitlab.io/pyxel/doc/latest/background/running_modes/observation_mode.html) or about the libraries\n",
    "[Xarray](http://xarray.pydata.org/en/stable/),\n",
    "[matplotlib](https://matplotlib.org/stable/index.html)\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Tip:</b> Open the <i>observation.yaml</i> file in parallel for a better overview.</div>\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a505cd0d-09ca-4b85-badc-a51702bfa357",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import xarray as xr\n",
    "\n",
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a4ef7c0-99ec-4732-8d99-ebfc5f4829a3",
   "metadata": {},
   "source": [
    "## Loading the configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9518cd05-eea7-4772-b5dc-084315aa68be",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a configuration class from the yaml file\n",
    "config = pyxel.load(\"observation.yaml\")\n",
    "\n",
    "observation = config.observation  # class Observation\n",
    "detector = config.detector  # class CCD\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "957dc166-e3fc-4942-98d5-f299cee31538",
   "metadata": {},
   "source": [
    "## Running the pipelines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9ea9520-9eab-44d1-80d5-051d431517c3",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.observation_mode(\n",
    "    observation=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "829f285d-01e4-4bab-b3c4-0fba67556212",
   "metadata": {},
   "outputs": [],
   "source": [
    "result.dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e143a5b-e0fb-4616-af16-5b8791fdee8c",
   "metadata": {},
   "source": [
    "# Plotting the outputs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e02308d2-95d6-40ee-9c42-ab5a505b23ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a72824c-03e6-46a7-96d0-9eb9da7c703f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# define dark current from pixel dataset\n",
    "dark_current = result.dataset.pixel.mean(dim=[\"x\", \"y\"]).to_numpy()\n",
    "temperature = result.dataset.temperature.to_numpy()\n",
    "# dark current without spatial noise\n",
    "dark_current_no_fpn = (\n",
    "    result.dataset.sel(spatial_noise_factor=0.0)\n",
    "    .pixel.mean(dim=[\"x\", \"y\"])\n",
    "    .to_numpy()\n",
    ")\n",
    "# dark current with pattern noise and figure of merit=0.01\n",
    "dark_current_fpn = (\n",
    "    result.dataset.sel(figure_of_merit=1).pixel.mean(dim=[\"x\", \"y\"]).to_numpy()\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60a0c0c3-72f0-4d57-ad90-f2d5c81cfaf9",
   "metadata": {},
   "source": [
    "## Different dark current figure of merit values\n",
    "Plot 1 will show the dark current model used in Pyxel against the temperature for different figure of merit values. It is the dark current figure of merit at 300 K in nA/cm^2. \n",
    "\n",
    "- A higher value for the figure of merit means that the dark current is higher and it will reach the full-well capacity (FWC) already at lower temperatures in comparison to a small figure of merit value.\n",
    "- FPN is set to 0, so no spatial noise is included."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "439dc9db-a0b0-4aef-97a9-aef0346385c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(1)\n",
    "for i in range(0, len(result.dataset.figure_of_merit)):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current_no_fpn[i],\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"{result.dataset.figure_of_merit.values[i]}\" r\" $\\mathrm{nA/cm^2}$\",\n",
    "    )\n",
    "plt.title(\"Dark current vs. temperature for different figure of merit\")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83882530-7945-4fdd-941b-fbb3178dedd1",
   "metadata": {},
   "source": [
    "### Comparison of result from Pyxel simnulation to literature\n",
    "Below is a figure from the book [\"Scientific Charge-Couple Devices.\" by Janesick, J. (2001)](https://spie.org/Publications/Book/374903?SSO=1), p.625. It shows the same plot like above, but the unit of the tempeature is Celsius instead of Kelvin and without FWC."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a7694d0-dd70-47af-a700-aa83f6076c85",
   "metadata": {},
   "source": [
    "<img style=\"float: center;\" src=\"images/Dc_T_fom.png\" width=\"300\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a54e62cf-0d25-4e02-8f8e-098a1ae3bfa0",
   "metadata": {},
   "source": [
    "## Different fixed-pattern noise factors\n",
    "Plot 2 will show the dark current model used in Pyxel against the temperature for different FPN values. \n",
    "    \n",
    "- The dark current at 300 K is 0.01 nA/cm^2. \n",
    "- Only at high temperatures (or for long exposure times) the FPN plays a role. \n",
    "- Higher values result in higher dark current values and an earlier saturation of the pixel to the FWC.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Try:</b> You can set temporal noise to <b>true</b> in the <i>yaml</i> file, run everything again and see the difference in the plots, if temporal noise is included. </div>\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36ded45e-3f9e-447e-b6ca-496561b1df62",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(2)\n",
    "for i in range(0, len(result.dataset.spatial_noise_factor)):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current_fpn[i],\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"{result.dataset.spatial_noise_factor.values[i]}\",\n",
    "    )\n",
    "plt.title(\"Dark current vs. temperature for different FPN\")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6098e02f-2741-4438-ac1e-3d21038c7cbd",
   "metadata": {},
   "source": [
    "## Comparison of result with two different approximations\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dcae36e6-6410-4ed2-aba0-61dcfe19c6ea",
   "metadata": {},
   "source": [
    "**Approx1**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f6b43d37-679a-4322-844f-b470af01dc87",
   "metadata": {},
   "outputs": [],
   "source": [
    "# between 230 and 300 K valid\n",
    "def dark_current_approx1(result):\n",
    "    T = np.arange(230.0, 300.0, 1.0)\n",
    "    Dc293 = 250  # e/pixel/s\n",
    "    Dc = (1.14e06 * T**3 * np.exp(-9080 / T)) * Dc293\n",
    "\n",
    "    return Dc, T"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94ad543b-7f8c-462e-aa1c-a10cd707839f",
   "metadata": {},
   "source": [
    "**Approx2**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0aabd5ae-a9dd-400c-8873-a41628ce4653",
   "metadata": {},
   "outputs": [],
   "source": [
    "# between 230 and 300 K valid\n",
    "def dark_current_approx2(result):\n",
    "    T = np.arange(230.0, 300.0, 1.0)\n",
    "    Dc293 = 20000  # e/pixel/s\n",
    "    Dc = (122 * T**3 * np.exp(-6400 / T)) * Dc293\n",
    "\n",
    "    return Dc, T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bff003e1-8250-4312-9543-1d0438a19121",
   "metadata": {},
   "outputs": [],
   "source": [
    "Dc_approx1, T = dark_current_approx1(result)\n",
    "Dc_approx2, T = dark_current_approx2(result)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8dbc9d1-3092-40d5-aba8-1b92ea5fe1ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(1)\n",
    "for i in range(0, len(result.dataset.figure_of_merit)):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current_no_fpn[i],\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"{result.dataset.figure_of_merit.values[i]}\" r\" $\\mathrm{nA/cm^2}$\",\n",
    "    )\n",
    "plt.semilogy(T, Dc_approx1, \"-\", color=\"brown\", ms=5, mfc=\"none\", label=\"Approx 1\")\n",
    "plt.semilogy(T, Dc_approx2, \"-\", color=\"black\", ms=5, mfc=\"none\", label=\"Approx 2\")\n",
    "plt.semilogy(293, 250, \"*\", color=\"brown\", label=\"Typical value for Approx 1\")\n",
    "plt.semilogy(\n",
    "    293,\n",
    "    20000,\n",
    "    \"*\",\n",
    "    color=\"black\",\n",
    "    label=\"Typical value for Approx 2\",\n",
    ")\n",
    "plt.title(\n",
    "    \"Dark current vs. temperature for different figure of merit values in comparison to two approximations\"\n",
    ")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.legend(loc=\"center left\", bbox_to_anchor=(1, 0.5))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ff26018-791b-4f2f-8de7-8ef58bfd62c4",
   "metadata": {},
   "source": [
    "## Plotting image as function of readout time, temperature and figure of merit\n",
    "\n",
    "Library `holoviews` can be used to quickly plot data stored inside `xarray` datasets.\n",
    "Here with the `bokeh` backend, `matplotlib` is also supported.\n",
    "\n",
    "You can use the toolbar to change temperature and figure of merit to see the influence on the detector.\n",
    "\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Try:</b> You can change the <b>readout time</b> in the <i>yaml</i> file, reload the configuration and run the pipeline again. Then display the results and see the influence of the new readout time.</div>\n",
    "\n",
    "Longer readout times result in higher dark current and saturation is reached at lower temperatures already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9bf778cc-684c-420f-8df7-324068bf580e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts\n",
    "\n",
    "hv.extension(\"bokeh\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "143c409d-f354-44fb-8ce5-63b2547067ac",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "def display_array(data: xr.DataArray, color_map=\"gray\", num_bins=100) -> \"Layout\":\n",
    "    \"\"\"Display detector interactively.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    detector: Detector\n",
    "    hist: bool\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    hv.Layout\n",
    "        A Holoviews object.\n",
    "    \"\"\"\n",
    "\n",
    "    # Late import to speedup start-up time\n",
    "    import holoviews as hv\n",
    "\n",
    "    # Apply an extension to Holoviews (if needed)\n",
    "    if not hv.Store.renderers:\n",
    "        hv.extension(\"bokeh\")\n",
    "\n",
    "    assert \"y\" in data.dims\n",
    "    assert \"x\" in data.dims\n",
    "\n",
    "    other_dims = [name for name in data.dims if name not in (\"y\", \"x\")]\n",
    "\n",
    "    dct = {key: data[key].values for key in other_dims}\n",
    "\n",
    "    min_value = float(data.min())\n",
    "    max_value = float(data.max())\n",
    "\n",
    "    def _get_image(*params: tuple):\n",
    "\n",
    "        selected_coords = dict(zip(other_dims, params))\n",
    "        selected_data = data.sel(**selected_coords)\n",
    "\n",
    "        num_y, num_x = selected_data.shape\n",
    "\n",
    "        return hv.Image(selected_data).opts(\n",
    "            # clim=(min_value, max_value),\n",
    "            colorbar=True,\n",
    "            cmap=color_map,\n",
    "            tools=[\"hover\"],\n",
    "            aspect=num_y / num_x,\n",
    "            invert_yaxis=True,\n",
    "        )\n",
    "\n",
    "    dmap = (\n",
    "        hv.DynamicMap(_get_image, kdims=other_dims)\n",
    "        .redim.values(**dct)\n",
    "        .opts(framewise=True)\n",
    "    )\n",
    "\n",
    "    hist = dmap.hist(adjoin=False, num_bins=num_bins).opts(\n",
    "        aspect=1.5, framewise=True, tools=[\"hover\"], xlabel=\"z\"\n",
    "    )\n",
    "\n",
    "    out = (dmap.relabel(\"Array\") + hist.relabel(\"Histogram\")).opts(tabs=True)\n",
    "\n",
    "    return out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c529234e-0508-41cf-a39c-90032ae04877",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_array(result.dataset[\"pixel\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b8253637-225f-4a61-acee-a2e5c2235c39",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "97d4ec55-0203-40a8-9594-593c213739d6",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "toc-autonumbering": true,
  "toc-showcode": true,
  "toc-showmarkdowntxt": true,
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {
     "0308519a80674238af2d435074a716a1": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_2035db66dfe44d2e94456de7a2076e21",
       "style": "IPY_MODEL_a7d1502a20fd46ebbba92c9f7e608739",
       "value": "  0%"
      }
     },
     "0abb98c629474c63ab169b1f01c004ed": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "FloatProgressModel",
      "state": {
       "bar_style": "success",
       "layout": "IPY_MODEL_9537fe846a1a49cb8a49f7d171efe9e8",
       "max": 32,
       "style": "IPY_MODEL_bcb81ef9f8274044b32aa4589a216eb1",
       "value": 32
      }
     },
     "0d2f6c3a36694a23a3fad84849ce274d": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "FloatProgressModel",
      "state": {
       "bar_style": "danger",
       "layout": "IPY_MODEL_f56b5e8ffa264725ae16f6b598fd00a1",
       "max": 32,
       "style": "IPY_MODEL_a4aa03b20cc149e9a7df213e0d083397"
      }
     },
     "0d3926ba26654542b747b29a0e49f521": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_c6d8af2fd48f489499143360222e4cb0",
       "style": "IPY_MODEL_57c0026a9d4c4432b01fd6869c41e51b",
       "value": "  0%"
      }
     },
     "0dc502d034264ce19e125e1a0f77670d": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "179b4e977e4d411ba0b3e0948669b603": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "FloatProgressModel",
      "state": {
       "bar_style": "danger",
       "layout": "IPY_MODEL_eb644aaf21f94353aaacba7348f01254",
       "max": 32,
       "style": "IPY_MODEL_438c19ae643c49f6af07cf6d9633f6d5"
      }
     },
     "1d52100182e8405c97b2a7e62e3b0c3d": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_61abbc6c61124535b7f117639d9a1ccb",
       "style": "IPY_MODEL_f83dc928c3234b5e98ca67f9ffca88eb",
       "value": " 32/32 [00:00&lt;00:00, 127.33it/s]"
      }
     },
     "2035db66dfe44d2e94456de7a2076e21": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "230f2dfcf6a549f79226fd11e7a02d6d": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "281b47f74f7c4130a5dd2a260eb5d2b2": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "2a6769abfe564ccdb3d7d95ef6407092": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "37b34801ef9c47b08a7b36c3c6a8e8f3": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HBoxModel",
      "state": {
       "children": [
        "IPY_MODEL_4848aa9bed664dbbaca2f9b2c02df0de",
        "IPY_MODEL_0abb98c629474c63ab169b1f01c004ed",
        "IPY_MODEL_1d52100182e8405c97b2a7e62e3b0c3d"
       ],
       "layout": "IPY_MODEL_9e6db1ebf70949789b57ad25cf663cf6"
      }
     },
     "38eeaea61645485787254729baf106fa": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HBoxModel",
      "state": {
       "children": [
        "IPY_MODEL_59ebcf9b691e45e9992ac738adc51a95",
        "IPY_MODEL_8c388d4bf1f343a69512f032bd278ae7",
        "IPY_MODEL_7fcab30ec98a456282a8517072515c0e"
       ],
       "layout": "IPY_MODEL_e2f9fd8be09a4a6cb594c819c4d4e8bf"
      }
     },
     "3b3d85edc699412fa7ece350a31b8f26": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "FloatProgressModel",
      "state": {
       "bar_style": "success",
       "layout": "IPY_MODEL_50318e9053ee49c0adbfbfa0559b43f1",
       "max": 32,
       "style": "IPY_MODEL_9f115976d4e34a319433b0a6ac3c1cce",
       "value": 32
      }
     },
     "3ffd559ee6524ab39d8bbc659bf5c3e5": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "4374fd290b1642b4b482b69a3e055fda": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_e628bdba445d452cb14282aeb1eb0a2f",
       "style": "IPY_MODEL_4f688cc4364543b5a241ffe56e5d2162",
       "value": "100%"
      }
     },
     "438c19ae643c49f6af07cf6d9633f6d5": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "ProgressStyleModel",
      "state": {
       "description_width": ""
      }
     },
     "45186632b9a441e9b5f3d183b3ab6c8d": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "45e99e0d226e4e3197fa227afde29034": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "4848aa9bed664dbbaca2f9b2c02df0de": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_ac64d257bf98483ab9117d2a1123e2f5",
       "style": "IPY_MODEL_c920b2a5881b484d89df8c0ad3c4cb0d",
       "value": "100%"
      }
     },
     "4f688cc4364543b5a241ffe56e5d2162": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "50318e9053ee49c0adbfbfa0559b43f1": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "546956de524c4a3ab6b9f667ae310654": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_281b47f74f7c4130a5dd2a260eb5d2b2",
       "style": "IPY_MODEL_3ffd559ee6524ab39d8bbc659bf5c3e5",
       "value": " 960/960 [00:08&lt;00:00, 124.20it/s]"
      }
     },
     "5773f9208b144515844576f97bfd97f5": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HBoxModel",
      "state": {
       "children": [
        "IPY_MODEL_0308519a80674238af2d435074a716a1",
        "IPY_MODEL_179b4e977e4d411ba0b3e0948669b603",
        "IPY_MODEL_5cea37b9264940a78eb7e46219e42df4"
       ],
       "layout": "IPY_MODEL_45186632b9a441e9b5f3d183b3ab6c8d"
      }
     },
     "57c0026a9d4c4432b01fd6869c41e51b": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "59ebcf9b691e45e9992ac738adc51a95": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_a352f92cf1294e89bbe3c20265806306",
       "style": "IPY_MODEL_f87aaad4031444cfafb23ac23d6001fd",
       "value": "100%"
      }
     },
     "5cea37b9264940a78eb7e46219e42df4": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_f93924ba25bb4f6da3cc69a30a5c73d7",
       "style": "IPY_MODEL_45e99e0d226e4e3197fa227afde29034",
       "value": " 0/32 [00:00&lt;?, ?it/s]"
      }
     },
     "5d15b8dac0494601b905577e14076a5d": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "ProgressStyleModel",
      "state": {
       "description_width": ""
      }
     },
     "5df001ce3c544e828fc5624d825b54cb": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "61abbc6c61124535b7f117639d9a1ccb": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "74a798e46d884ae5a8a9c5f91585c8b8": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "74fdc257e2e54aadaa9cbc3a59be795d": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "7fcab30ec98a456282a8517072515c0e": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_74a798e46d884ae5a8a9c5f91585c8b8",
       "style": "IPY_MODEL_c5943ff49fc248349355ef3b55965a1a",
       "value": " 32/32 [00:00&lt;00:00, 125.77it/s]"
      }
     },
     "8c388d4bf1f343a69512f032bd278ae7": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "FloatProgressModel",
      "state": {
       "bar_style": "success",
       "layout": "IPY_MODEL_8dcb4aa119c8418dad8b0c795142cef2",
       "max": 32,
       "style": "IPY_MODEL_f742ac59a9274dd8a07af3cb0de8adb2",
       "value": 32
      }
     },
     "8dcb4aa119c8418dad8b0c795142cef2": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "9537fe846a1a49cb8a49f7d171efe9e8": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "99baf9e4f0574f29bb2c36d63e8e0247": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "9dc64447a81e4497a36bd8bc7a0d3c84": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_ea8144e0e2e1487aab68dc17acd3c081",
       "style": "IPY_MODEL_0dc502d034264ce19e125e1a0f77670d",
       "value": " 32/32 [00:00&lt;00:00, 101.93it/s]"
      }
     },
     "9e6db1ebf70949789b57ad25cf663cf6": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "9f115976d4e34a319433b0a6ac3c1cce": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "ProgressStyleModel",
      "state": {
       "description_width": ""
      }
     },
     "a352f92cf1294e89bbe3c20265806306": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "a4aa03b20cc149e9a7df213e0d083397": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "ProgressStyleModel",
      "state": {
       "description_width": ""
      }
     },
     "a7d1502a20fd46ebbba92c9f7e608739": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "ac64d257bf98483ab9117d2a1123e2f5": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "b4a5612cb4014e26bac09ac0f6e81c30": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HBoxModel",
      "state": {
       "children": [
        "IPY_MODEL_b71eee6e13e144049c1d893213fb7b53",
        "IPY_MODEL_d31aabe56c7a4bb59875b5e5e5d9b305",
        "IPY_MODEL_546956de524c4a3ab6b9f667ae310654"
       ],
       "layout": "IPY_MODEL_74fdc257e2e54aadaa9cbc3a59be795d"
      }
     },
     "b6baa7277805492e81b1c07c3dc2760b": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HBoxModel",
      "state": {
       "children": [
        "IPY_MODEL_0d3926ba26654542b747b29a0e49f521",
        "IPY_MODEL_0d2f6c3a36694a23a3fad84849ce274d",
        "IPY_MODEL_bdf3a485e1fa4a63874e8e31151489cd"
       ],
       "layout": "IPY_MODEL_2a6769abfe564ccdb3d7d95ef6407092"
      }
     },
     "b71eee6e13e144049c1d893213fb7b53": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_99baf9e4f0574f29bb2c36d63e8e0247",
       "style": "IPY_MODEL_bcd8e94f42a64f2f9ea87ce3188eabf8",
       "value": "100%"
      }
     },
     "bcb81ef9f8274044b32aa4589a216eb1": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "ProgressStyleModel",
      "state": {
       "description_width": ""
      }
     },
     "bcd8e94f42a64f2f9ea87ce3188eabf8": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "bdf3a485e1fa4a63874e8e31151489cd": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLModel",
      "state": {
       "layout": "IPY_MODEL_5df001ce3c544e828fc5624d825b54cb",
       "style": "IPY_MODEL_c20af6e43f064409aaa4d0bcdf575c8d",
       "value": " 0/32 [00:00&lt;?, ?it/s]"
      }
     },
     "c0ffdfe7f77949e1b150423e22a87ec0": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HBoxModel",
      "state": {
       "children": [
        "IPY_MODEL_4374fd290b1642b4b482b69a3e055fda",
        "IPY_MODEL_3b3d85edc699412fa7ece350a31b8f26",
        "IPY_MODEL_9dc64447a81e4497a36bd8bc7a0d3c84"
       ],
       "layout": "IPY_MODEL_230f2dfcf6a549f79226fd11e7a02d6d"
      }
     },
     "c20af6e43f064409aaa4d0bcdf575c8d": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "c5943ff49fc248349355ef3b55965a1a": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "c6d8af2fd48f489499143360222e4cb0": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "c920b2a5881b484d89df8c0ad3c4cb0d": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "d31aabe56c7a4bb59875b5e5e5d9b305": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "FloatProgressModel",
      "state": {
       "bar_style": "success",
       "layout": "IPY_MODEL_f726ed90b0b142f78a8ab3307bf9f13f",
       "max": 960,
       "style": "IPY_MODEL_5d15b8dac0494601b905577e14076a5d",
       "value": 960
      }
     },
     "e2f9fd8be09a4a6cb594c819c4d4e8bf": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "e628bdba445d452cb14282aeb1eb0a2f": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "ea8144e0e2e1487aab68dc17acd3c081": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "eb644aaf21f94353aaacba7348f01254": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "f56b5e8ffa264725ae16f6b598fd00a1": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "f726ed90b0b142f78a8ab3307bf9f13f": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     },
     "f742ac59a9274dd8a07af3cb0de8adb2": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "ProgressStyleModel",
      "state": {
       "description_width": ""
      }
     },
     "f83dc928c3234b5e98ca67f9ffca88eb": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "f87aaad4031444cfafb23ac23d6001fd": {
      "model_module": "@jupyter-widgets/controls",
      "model_module_version": "2.0.0",
      "model_name": "HTMLStyleModel",
      "state": {
       "description_width": "",
       "font_size": null,
       "text_color": null
      }
     },
     "f93924ba25bb4f6da3cc69a30a5c73d7": {
      "model_module": "@jupyter-widgets/base",
      "model_module_version": "2.0.0",
      "model_name": "LayoutModel",
      "state": {}
     }
    },
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
