{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "source": [
    "```{admonition} Live Notebook\n",
    "You can run this notebook in a \n",
    "[live session ![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?labpath=examples%2Fexposure%2Fexposure.ipynb)\n",
    "or view it on [Gitlab](https://gitlab.com/esa/pyxel-data/-/blob/master/examples/exposure/exposure.ipynb).\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exposure mode\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../../images/pyxel_logo.png\" width=\"250\">\n",
    "\n",
    "This notebook is an example of a 'exposure' pipeline. \n",
    "\n",
    "Running Pyxel in Exposure mode can be used to get a one image or multiple images with different readout times with the detector effects defined in the configuration file.\n",
    "\n",
    "If you haven't installed Pyxel yet, visit https://esa.gitlab.io/pyxel/doc/tutorials/install.html for instructions.\n",
    "\n",
    "The YAML configuration file looks like that:\n",
    "\n",
    "```yaml\n",
    "# YAML configuration file for Exposure mode\n",
    "exposure:\n",
    "  \n",
    "  outputs:                        # Define parameters for output\n",
    "    output_folder: 'output'\n",
    "    save_data_to_file:\n",
    "      - detector.image.array:   [fits]\n",
    "    \n",
    "ccd_detector:                   # Define detector\n",
    "  ...\n",
    "    \n",
    "pipeline:                       # Define the pipeline\n",
    "  ...\n",
    "```\n",
    "This will create an output folder called \"output\" and create a histogram of the final image. Additionally, specifying `save_data_to_file parameter` will also save the output image in `.fits` format. Optionally, user can save additional information in different file formats, for example by adding additional parameters to `save_data_to_file`: \n",
    "```yaml\n",
    "- detector.signal.array:  [npy, txt]\n",
    "- detector.charge.frame:  [csv]\n",
    "- detector.photon.array:  [hdf]\n",
    "```\n",
    "If no save_data_to_file is specified, the default will be \n",
    "```yaml \n",
    "- detector.image.array:   [fits]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Running Pyxel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyxel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display current version of Pyxel\n",
    "print(\"Pyxel version:\", pyxel.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Open the example YAML file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"exposure.yaml\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display the configuration\n",
    "\n",
    "### Running mode\n",
    "Running mode class stores the mode parameters, the `Outputs` class. Beside single mode, the advanced usage modes are parametric, calibration and dynamic. We can display the configuration class separately using `display_html` from the `pyxel.notebook`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating detector and detection pipeline objects\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exposure = config.exposure\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Detector\n",
    "Detector object stores all the information about its properties, such as geometry, environment, material, characteristics, and all the processed data during the simulation, this data is described by the instances of the `Photon`, `Charge`, `Pixel`, `Signal` and `Image` classes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(detector)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pipeline\n",
    "Pipeline is an instance of the `DetectionPipeline` class and consists of model groups, each representing a stage of the measurement procedure, from photon generation to readout electronics. Each **model group** consists of multiple **model functions**, each specified in the `.yaml` file together with their parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(pipeline)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Displaying a single model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(pipeline.photon_collection.optical_psf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Processing the detector through the pipeline\n",
    "\n",
    "Now that the configuration is loaded, we have a detection pipeline object and a detector object. To start the exposure mode pipeline, we can run it by calling the function `exposure_mode`, passing the detector, pipeline and the instance of `Exposure` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.exposure_mode(exposure=exposure, detector=detector, pipeline=pipeline)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display detector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_detector(detector)"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
