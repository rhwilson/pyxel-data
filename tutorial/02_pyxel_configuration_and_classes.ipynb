{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "```{admonition} Live Notebook\n",
    "You can run this notebook in a \n",
    "[live session ![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?labpath=tutorial%2F02_pyxel_configuration_and_classes.ipynb)\n",
    "or view it on [Gitlab](https://gitlab.com/esa/pyxel-data/-/blob/master/tutorial/02_pyxel_configuration_and_classes.ipynb).\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pyxel configuration and classes\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../images/pyxel_logo.png\" width=\"250\">\n",
    "\n",
    "In this notebook we will take a closer look at the main Pyxel classes and inspect them along the YAML configuration file.\n",
    "\n",
    "By the end of the lesson you will know how to:\n",
    "* Inspect Pyxel objects\n",
    "* Edit the YAML configuration file\n",
    "\n",
    "## Table of contents\n",
    "1. [YAML basic syntax](#YAMLsyntax)\n",
    "2. [Inspecting Pyxel classes](#inspecting)\n",
    "    1. [Configuration](#configuration)\n",
    "    2. [Running mode](#runningmode)\n",
    "    3. [Detector](#detector)\n",
    "    4. [Pipeline](#pipeline)\n",
    "3. [Exercise 1](#exercise1)\n",
    "4. [Exercise 2](#exercise2)\n",
    "\n",
    "To inspect the objects, we will use the function `display_html` from Pyxel's `notebook` module. Let's start by loading the same configuration file as in the prevoius tutorial, `exposure.yaml`.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>Tip:</b> Open the exposure.yaml file in parallel for a better overview.</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyxel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"exposure.yaml\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='YAMLsyntax'></a>\n",
    "## YAML basic syntax\n",
    "\n",
    "A quick overview of possible inputs and structures in the YAML file.\n",
    "\n",
    "**Numbers**\n",
    "```YAML\n",
    "one:  1.\n",
    "two:   3.e-6\n",
    "three:  10\n",
    "```\n",
    "\n",
    "**Strings**\n",
    "```YAML\n",
    "string: foo\n",
    "forced_string: \"bar\"\n",
    "```\n",
    "\n",
    "**Lists**\n",
    "```YAML\n",
    "list: [1,2]\n",
    "\n",
    "or\n",
    "\n",
    "list:\n",
    "  - 1\n",
    "  - 2\n",
    "```\n",
    "\n",
    "**Dictionaries**\n",
    "```YAML\n",
    "dictionary: {\"foo\":1, \"bar\":2}\n",
    "\n",
    "or\n",
    "\n",
    "dictionary:\n",
    "  foo: 1\n",
    "  bar: 2\n",
    "```\n",
    "\n",
    "**Comments**\n",
    "```YAML\n",
    "# just a comment\n",
    "```\n",
    "\n",
    "**Example**\n",
    "```YAML\n",
    "foo:\n",
    "  - 1\n",
    "  - 2  \n",
    "bar:\n",
    "  one:\n",
    "    - alpha\n",
    "    - \"beta\"\n",
    "  two: 5.e-3\n",
    "  \n",
    "would be converted to\n",
    "\n",
    "{\"foo\":[1,2], \"bar\":{'one':[\"alpha\", \"beta\"], \"two\":5.e-3}}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='inspecting'></a>\n",
    "## Inspecting Pyxel classes\n",
    "\n",
    "When the YAML configuration file is loaded, the nested dictionaries, lists, numbers and strings are used to construct Pyxel objects. These can be inspected by the function `display_html()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='configuration'></a>\n",
    "### Configuration\n",
    "\n",
    "\n",
    "Let's first take a look at the `config` object, an instance of the class `Configuration`. Since we specify in the YAML configuration that we are working with single mode and CCD detector, only this attributes are different to None. Running mode attribute `exposure` is an instance of `Exposure` class, but for other running modes it could be: `Observation` or `Calibration`. `ccd_detector` attribute is an instance of `CCD` class, but it could also be `CMOS`. And last, `pipeline` is always an instance of `DetectionPipeline`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To run the single mode simulation with a CCD detector we need the following three:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exposure = config.exposure  # class Exposure\n",
    "detector = config.detector  # class CCD\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='runningmode'></a>\n",
    "### Running mode\n",
    "\n",
    "In the case of single mode, the running mode `Exposure` is a simple class, containing only the `ExposureOutputs`. The YAML input looks like this, no other parameters are specified beside outputs:\n",
    "\n",
    "```YAML\n",
    "exposure:\n",
    "\n",
    "  outputs:\n",
    "    output_folder: \"exposure_output\"\n",
    "    save_data_to_file:\n",
    "      - detector.image.array:   ['fits']\n",
    "      - detector.pixel.array: ['npy']\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(exposure)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(exposure.outputs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='detector'></a>\n",
    "### Detector\n",
    "\n",
    "The YAML input is structured in three parts, specifying properties geometry, environment and characteristics:\n",
    "```YAML\n",
    "ccd_detector:\n",
    "\n",
    "  geometry:\n",
    "\n",
    "    row: 450               # pixel\n",
    "    col: 450               # pixel \n",
    "    ...\n",
    "\n",
    "  environment:\n",
    "    temperature: 300        # K\n",
    "\n",
    "  characteristics:\n",
    "    quantum_efficiency:   1.                # -\n",
    "    ...\n",
    "```\n",
    "\n",
    "Beside those three, after initialization the detector also holds other information such as simulated data, input image and temporal properties for dynamic running mode. in the simulated data part we find classes like `Photon`, `Charge`, `Pixel`, `Signal` and `Image`. These classes get edited by the models when running the pipeline. Let's see below how detector looks before the simulation. Note that some of the attributes are not initialized yet at this stage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(detector)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='pipeline'></a>\n",
    "### Pipeline\n",
    "\n",
    "A section from the YAML configuration:\n",
    "```YAML\n",
    "pipeline:\n",
    "  # -> photon\n",
    "  ...\n",
    "\n",
    "  # photon -> photon\n",
    "  optics:\n",
    "    - name: optical_psf\n",
    "      func: pyxel.models.optics.optical_psf\n",
    "      enabled: true\n",
    "      arguments:\n",
    "        fov_arcsec: 5 # FOV in arcseconds\n",
    "        pixelscale: 0.01 #arcsec/pixel\n",
    "        wavelength: 0.6e-6 # wavelength in meters\n",
    "        optical_system:\n",
    "          - item: CircularAperture\n",
    "            radius: 3.0\n",
    "  ...    \n",
    "```\n",
    "The Pyxel detection pipeline is split into multiple sections, each describing a stage of the detection process, for example photon generation, optics, charge generation... These are called model groups, and are instances of `ModelGroup` class. Each of them can hold a list of multiple models, functions that edit the detector, instances of `ModelFunction`. For each model function we specify in the YAML an arbitrary name, the path to the function in Pyxel, a boolean for enabled/disabled and all the necessary arguments.\n",
    "\n",
    "Before each model group there is a comment in the configuration file to note which detector data is edited. For example `# photon -> charge`, indicates that the models in the model group convert photons to charge.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(pipeline)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='exercise1'></a>\n",
    "## Exercise 1\n",
    "\n",
    "Use the `display_html` function on instances of `ModelGroup` or `ModelFunction` classes. \n",
    "\n",
    "**Tip:** You can access the model `cdm` from the model group `charge_transfer` like so: `pipeline.charge_transfer.cdm`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See the solutions below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# ModelGroup\n",
    "pyxel.display_html(pipeline.charge_transfer)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# ModelFunction\n",
    "pyxel.display_html(pipeline.charge_transfer.cdm)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pipeline.charge_transfer.cdm.arguments.beta = 0.4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(pipeline.charge_transfer.cdm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='exercise1'></a>\n",
    "## Exercise 2\n",
    "\n",
    "Try changing parameters in the configuration file, reload the configuration and run single mode with the changed parameters and display the results. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See the solution below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "config = pyxel.load(\"exposure.yaml\")\n",
    "\n",
    "pyxel.exposure_mode(\n",
    "    exposure=config.exposure,\n",
    "    detector=config.ccd_detector,\n",
    "    pipeline=config.pipeline,\n",
    ")\n",
    "\n",
    "pyxel.display_detector(detector)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc-autonumbering": false,
  "toc-showcode": false,
  "toc-showmarkdowntxt": false,
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
