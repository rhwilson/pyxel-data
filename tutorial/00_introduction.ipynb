{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "source": [
    "```{admonition} Live Notebook\n",
    "You can run this notebook in a \n",
    "[live session ![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?labpath=tutorial%2F00_introduction.ipynb)\n",
    "or view it on [Gitlab](https://gitlab.com/esa/pyxel-data/-/blob/master/tutorial/00_introduction.ipynb).\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../images/pyxel_logo.png\" width=\"350\">\n",
    "\n",
    "Welcome to the Pyxel tutorial.\n",
    "\n",
    "Pyxel is a general detector Python simulation framework. It is novel, open-source and modular, designed to host and pipeline models (analytical, numerical, statistical) simulating different types of detector effects on images produced by Charge-Coupled Devices (CCD), Monolithic, and Hybrid CMOS imaging sensors.\n",
    "\n",
    "Users can provide one or more input images to Pyxel, set the detector and model parameters via a configuration file and select which effects to simulate: cosmic rays, detector Point Spread Function (PSF), electronic noises, Charge Transfer Inefficiency (CTI), persistence, dark current, charge diffusion, optical effects, etc. The output is one or more images including the simulated detector effects combined. On top of its model hosting capabilities, the framework also provides a set of basic image analysis tools. It also features a parametric mode to perform parametric and sensitivity analysis, and a model calibration mode to find optimal values of its parameters based on a target dataset the model should reproduce.\n",
    "\n",
    "A majority of Pyxel users are expected to be detector scientists and engineers working with instruments - using detectors - built for astronomy and Earth observation, who need to perform detector simulations, for example to understand laboratory data, to derive detector design specifications for a particular application, or to predict instrument and mission performance based on existing detector measurements.\n",
    "\n",
    "One of the main purposes of this new tool is to share existing resources and avoid duplication of work. For instance, detector models developed for a certain project could be reused by other projects as well, making knowledge transfer easier.\n",
    "\n",
    "This collection of Jupyter notebooks is a part of the separate Pyxel example repository, called [Pyxel Data](https://gitlab.com/esa/pyxel-data) (**gitlab.com/esa/pyxel-data**).\n",
    "\n",
    "## Setup\n",
    "\n",
    "The examples can be run in the cloud without prior installation or downloading using [Binder](https://mybinder.org/), simply by clicking on the badge:\n",
    "[here](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?urlpath=lab/tree/single/basic):\n",
    "\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?labpath=tutorial%00_introduction.ipynb)\n",
    "\n",
    "If you decide to try Pyxel locally, you will need to install the Pyxel package and download the examples, following the steps:\n",
    "\n",
    "1. Install Pyxel:\n",
    "\n",
    "   Depending on your prefered way of installation (PyPI, Conda...), follow the [Installation guide in the Pyxel documentation](https://esa.gitlab.io/pyxel/doc/tutorials/install.html).\n",
    "   \n",
    "2. Once you’ve installed Pyxel, you can either download examples directly from [Pyxel Data](https://gitlab.com/esa/pyxel-data) or get a copy of all the examples in the repository by running the commands:\n",
    "\n",
    "   ```console\n",
    "   $ pyxel --download-examples\n",
    "   ```\n",
    "\n",
    "3. Start a Jupyter session:\n",
    "\n",
    "   ```console\n",
    "   $ jupyter lab\n",
    "   ```\n",
    "\n",
    "## Useful links\n",
    "\n",
    "1. Pyxel webpage and blog\n",
    "- [Webpage](https://esa.gitlab.io/pyxel/)\n",
    "\n",
    "2. References\n",
    "\n",
    "- [Documentation](https://esa.gitlab.io/pyxel/doc/)\n",
    "- [Code Repository](https://gitlab.com/esa/pyxely)\n",
    "- [Examples Repository](https://gitlab.com/esa/pyxel-data)\n",
    "- Pyxel paper ([Arxiv](https://arxiv.org/abs/2012.05006), [SPIE presentation](https://www.spiedigitallibrary.org/conference-proceedings-of-spie/11454/2561731/content/10.1117/12.2561731.full?SSO=1))\n",
    "\n",
    "3. Ask for help:\n",
    "\n",
    "- [Gitter channel](https://gitter.im/pyxel-framework/community) for general discussion\n",
    "- [GitLab Issues](https://gitlab.com/esa/pyxel/-/issues) for bug reports and\n",
    "  feature requests\n",
    "- [Google group](https://groups.google.com/g/pyxel-detector-framework)\n",
    "\n",
    "## Tutorial structure\n",
    "\n",
    "The tutorial is made up of multiple Jupyter Notebooks. These notebooks mix\n",
    "code, text, and visualization.\n",
    "\n",
    "The layout of the tutorial is as follows:\n",
    "\n",
    "1. [First simulation](01_first_simulation.ipynb)\n",
    "2. [Pyxel configuration and classes](02_pyxel_configuration_and_classes.ipynb)\n",
    "3. [Adding a model](03_create-model.ipynb)\n",
    "4. [Observation mode](04_observation_mode.ipynb)\n",
    "5. [Calibration mode](05_calibration_mode.ipynb)\n",
    "6. [Calibration outputs visualization](06_calibration_visualization.ipynb)\n",
    "7. [Exposure with multiple readouts](07_exposure_with_multiple_readouts.ipynb)\n",
    "\n",
    "## First step\n",
    "\n",
    "If you haven't used JupyterLab before, it's similar to the Jupyter Notebook. If\n",
    "you haven't used the Notebook, the quick intro is\n",
    "\n",
    "1. There are two modes: command and edit.\n",
    "2. From command mode, press `Enter` to edit a cell (like this markdown cell).\n",
    "3. From edit mode, press `Esc` to change to command mode.\n",
    "4. Press `Shift`+`Enter` to execute a cell and move to the next cell.\n",
    "5. The toolbar has commands for executing, converting, and creating cells.\n",
    "\n",
    "Execute the following cell and import Pyxel!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyxel\n",
    "\n",
    "pyxel.__version__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display version of Pyxel and its dependencies\n",
    "pyxel.show_versions()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
